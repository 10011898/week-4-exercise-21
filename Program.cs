﻿using System;

namespace week_4_exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("for loop:");
            for(var i=0; i<=5; i++)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine();
            
            Console.WriteLine("while loop:");
            var w = 0;
            while(w<5)
            {
                Console.WriteLine(w);
                w++;
            }
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("for while loop (only even numbers)");
            var w2 = 0;
            for(var fw = 0; fw <= 20; fw++)
            {
                 while(fw==w2)
                 {
                     Console.WriteLine(w2);
                     w2+=2;
                 }
            }
            Console.WriteLine();

            Console.WriteLine("do while loop");
            var d = 0;
            do
            {
                Console.WriteLine(d);
                d++;
            }while(d<5);
        }
    }
}
